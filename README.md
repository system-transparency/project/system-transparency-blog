# System Transparency Blog
This website is built using [Hugo](https://gohugo.io/) and the
[kiera-hugo](https://github.com/funkydan2/hugo-kiera) theme.

## Quick start after cloning
1. Run `git submodule update --init --recursive`
2. Follow the instruction [here](https://gohugo.io/getting-started/installing/)
to install Hugo.
3. Try serving the website locally
	1. Run `hugo serve`
	2. Browse http://localhost:1313

## Create a new story
Blog posts live in the `content/posts` directory.  You can use Hugo to generate
a basic template (`hugo new posts/mypost.md`), or simply peak at a previous
post.

## Generate a new website that can be published
1. Check that it runs as expected locally (see quick start, step 3).
2. Run `hugo`.  This will update the `public` repository, which is a git
submodule.
3. Navigate to `public`.
	1. Add all changes: `git add .`
	2. Commit: `git commit`
	3. Push: `git push origin master`
4. Navigate back to this repository and add/commit/push the new submodule hash
as well.
